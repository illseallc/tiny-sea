import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import github from '../img/github-icon.svg';
import 'bulma';

const Navbar = () => (
  <nav className="navbar is-primary">
    <div className="container">
      <div className="navbar-brand">
        <Link to="/" className="navbar-item">
          ILL SEA LLC
        </Link>
      </div>
      <div className="navbar-start">
        <Link className="navbar-item" to="/portfolio">
          Portfolio
        </Link>
      </div>
      <div className="navbar-end">
        <a className="navbar-item" href="https://github.com/illsea" target="_blank">
          <span className="icon">
            <img src={github} alt="Github" />
          </span>
        </a>
      </div>
    </div>
  </nav>
);

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet title="Home | ILL SEA LLC" />
    <Navbar />
    <div>{children()}</div>
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func
};

export default TemplateWrapper;
