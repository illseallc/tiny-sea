import React from 'react'

const NotFoundPage = () => (
  <div>
    <h1>NOT FOUND</h1>
    <p>A figment of your imagination, this page doesn&#39;t exist.</p>
  </div>
)

export default NotFoundPage
